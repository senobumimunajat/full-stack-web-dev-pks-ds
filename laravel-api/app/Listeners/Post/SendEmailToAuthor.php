<?php

namespace App\Listeners\Post;

use App\Event\PostCreated;
use App\Mail\PostCreatedMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToAuthor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostCreated  $event
     * @return void
     */
    public function handle(PostCreated $event)
    {
        // dd($event->post->user->email);
        Mail::to($event->post->user->email)->send(new PostCreatedMail($event->post));
    }
}
