<?php

namespace App\Listeners\Post;

use App\Event\RegisterCreated;
use App\Mail\RegisterCreatedMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailToNewUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterCreated  $event
     * @return void
     */
    public function handle(RegisterCreated $event)
    {
        //   dd($event->user->otp_code->otp);
          Mail::to($event->user->email)->send(new RegisterCreatedMail($event->user));
    }
}
