<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Event\RegisterCreated;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        // dd('masuk');
        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // Delete OTP Code Lama
        $user = User::where('email', $request->email)->first();
        if($user->otp_code) {
            $user->otp_code->delete();
        };
        
        // Create Otp Code Baru
        do {
            // $user->otp_code->delete();
            $random = mt_rand(100000,999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);
        $now = Carbon::now();
        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(15),
            'user_id' => $user->id
        ]);

        // Update Otp Code Baru ke JSON user
        $user = User::where('email', $request->email)->first();
        if($user->otp_code) {
            $user->otp_code->update();
        };
      
        // Kirim email otp code ke email user
        event(new RegisterCreated($user));
        //success save to database
        if($user) {
            // $user = User::with('otp_code')->where('id', $user_id)->get();

            return response()->json([
                'success' => true,
                'message' => 'User Created, OTP send to your email',
                'data'    => [
                    'user' => $user,
                    'otp_code' => $otp_code
                ] 
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'User Failed to Save',
        ], 409);
    }
}
