<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('update password');
        $validator = Validator::make($request->all(), [
            'email'   => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

      
        
        $user = User::where('email', $request->email)->first();
        if(!$user->email_verified_at) {
            return response()->json([
                'success' => false,
                'message' => 'please verified your account',
            ], 409);
        }
        $user -> update([
            'password' => Hash::make($request->password)
        ]);


        //success save to database
        if($user) {

            return response()->json([
                'success' => true,
                'message' => 'Password has been Update',
                'data'    => $user,
                
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'User Failed to Save',
        ], 409);

    }
}
