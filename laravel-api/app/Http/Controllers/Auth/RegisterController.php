<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Event\RegisterCreated;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('masuk');
         //set validation
         $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username'

        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $user = User::create($request->all());
        
        do {
            $random = mt_rand(100000,999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);
        $now = Carbon::now();
        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(15),
            'user_id' => $user->id
        ]);

        // Kirim email otp code ke email user
        // event(new RegisterCreated($user));
        //success save to database
        if($user) {

            return response()->json([
                'success' => true,
                'message' => 'User Created',
                'data'    => [
                    'user' => $user,
                    'otp_code' => $otp_code
                ] 
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'User Failed to Save',
        ], 409);

    
    }
}
