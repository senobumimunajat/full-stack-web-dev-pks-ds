<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('masuk verif');
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otpCode = OtpCode::where('otp', $request->otp)->first();
        if (!$otpCode) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code Not Found!'
            ], 404);
        }

        if ($otpCode->valid_until < Carbon::now()){
            return response()->json([
                'success' => false,
                'message' => 'OTP Code is Expired!'
            ], 400);
        }
        
        $user = $otpCode->user;
        $user->update([
            'email_verified_at' => Carbon::now()
        ]);
        $otpCode->delete();

        return response()->json([
            'success' => true,
            'message' => 'Conggrats! Your Account is Verified Now!',
            'data'    => $user
        ], 201);

    }
}
