<?php

namespace App\Http\Controllers;

use App\Comments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           //get data from table comments
           $comment = Comments::latest()->get();

           //make response JSON
           return response()->json([
               'success' => true,
               'message' => 'List Data Comments',
               'data'    => $comment  
           ], 200);
   
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //set validation
         $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = auth()->user();

        //save to database
        $comment = Comments::create([
            'content'     => $request->content,
            'post_id'     => $request->post_id,
            'user_id' => $user->id
        ]);

        //success save to database
        if($comment) {

            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data'    => $comment  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
              //find comment by ID
              $comment = Comments::findOrfail($id);

              //make response JSON
              return response()->json([
                  'success' => true,
                  'message' => 'Detail Data Post',
                  'data'    => $comment 
              ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comments $comment)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $comment = Comments::findOrFail($comment->id);

        if($comment) {
            $user = auth()->user();
            if($comment->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Its not your Comments',
                ], 403);
            }
            //update post
            $comment->update([
                'content'     => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comments Updated',
                'data'    => $comment  
            ], 200);

        }

        //data comment not found
        return response()->json([
            'success' => false,
            'message' => 'Comments Not Found',
        ], 404);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         //find Comment by ID
         $comment = Comments::findOrfail($id);
         if($comment) {
         $user = auth()->user();
            if($comment->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'You cant delete this comments! Its not your comments',
                ], 403);
            }
 
             //delete comment
             $comment->delete();
 
             return response()->json([
                 'success' => true,
                 'message' => 'Comments Deleted',
             ], 200);
 
            }
 
         //data comment not found
         return response()->json([
             'success' => false,
             'message' => 'Comments Not Found',
         ], 404);
    }
}
