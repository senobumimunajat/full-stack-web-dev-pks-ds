<?php

namespace App;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use UsesUuid;
    protected $fillable = ['title', 'description', 'user_id'];
    protected $primaryKey = 'id';

    public function comment()
    {
        return $this->hasMany('App\Comments');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
