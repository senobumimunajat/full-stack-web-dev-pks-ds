<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::middleware('auth:api')->get('/user', function (){
//     return auth()->user();
// });
// Route::group([
//     'middleware' => 'auth:api'], function() {   
//         Route::apiResource ([
//             'post' => 'PostController',
//             'comment' => 'CommentController'
//         ]);
    
// });
Route::middleware('auth:api')->group (function (){
    Route::apiResource('/post', 'PostController', ['except' => ['show', 'index']]);
    Route::apiResource('/comment', 'CommentController', ['except' => ['show', 'index']]);
});
Route::apiResource('/post', 'PostController', ['only' => ['show', 'index']]);
Route::apiResource('/comment', 'CommentController', ['only' => ['show', 'index']]);
Route::apiResource('/roles', 'RolesController');
Route::get('user', 'UserController');

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
] , function(){
    Route::Post('register', 'RegisterController')->name('auth.register');
    Route::Post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate');
    Route::Post('verification', 'VerificationController')->name('auth.verification');
    Route::Post('update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::Post('login', 'LoginController')->name('auth.login');
    Route::Post('logout', 'LogoutController')->name('auth.logout');
});
