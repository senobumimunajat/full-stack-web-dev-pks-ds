<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use App\Comments;
use Faker\Generator as Faker;

$factory->define(Comments::class, function (Faker $faker) {
    return [
        'content' => $faker->paragraph(),
        'post_id' => factory(App\Post::class),
        
    ];
});
