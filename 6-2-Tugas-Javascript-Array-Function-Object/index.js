// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var i = daftarHewan.sort();
console.log(daftarHewan);

// soal 2
function introduce({ name, age, address, hobby }) {
  return `Nama Saya ${name}, umur saya ${age} tahun, alamat saya di ${address} dan saya punya hobby yaitu ${hobby}`;
}

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" };
var perkenalan = introduce(data);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

// soal 3
function hitung_huruf_vokal(str) {
  menghitung = str.match(/[aiueo]/gi).length;
  return menghitung;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2); // 3 2

// soal 4
function hitung(nilai) {
  return nilai * 2 - 2;
}
console.log(hitung(0)); //-2
console.log(hitung(1)); //9
console.log(hitung(2)); //2
console.log(hitung(3)); //4
console.log(hitung(5)); //8
